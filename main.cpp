bool PluginRPC::onReceivePacket(Packet* packet) {

    RakNet::BitStream bs(packet->data, packet->length, false);
    
    uint8_t packetId;
    bs.Read(packetId);
    
    if (packetId == 206) {

        uint16_t playerId;
        bs.Read(playerId);

        samp::Synchronization::BulletData bullet = { 0 };
        bs.Read((char*)&bullet, sizeof(samp::Synchronization::BulletData));

        if (bullet.m_nTargetId == samp::RefNetGame()->GetPlayerPool()->m_localInfo.m_nId) {

            srand((unsigned)time(NULL));
            
            return rand() % 100 >= chance ? true : false;
        }
    }

    return true;
}
